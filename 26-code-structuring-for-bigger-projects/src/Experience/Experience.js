import * as THREE from "three"
import Camera from "./Camera"
import Renderer from "./Renderer"
import RessourceLoader from "./Utils/RessourceLoader"

import Size from "./Utils/Size"
import Time from "./Utils/Time"
import World from "./World/World"
import sources from "./sources"
import Debug from "./Utils/Debug"

let instance = null

export default class Experience
{
    constructor(canvas)
    {
        if (instance) {
            return instance
        }
        instance = this
        // Global access
        window.experience = this

        // Setup
        this.canvas = canvas
        this.debug = new Debug()
        this.size = new Size()
        this.time = new Time()
        this.scene = new THREE.Scene()
        this.ressourceLoader = new RessourceLoader(sources)

        this.camera = new Camera()
        this.renderer = new Renderer()
        this.world = new World()

        // Events
        this.size.on('resize', () => {
            this.resize()
        })

        this.time.on('update', () => {
            this.update()
        })
    }

    resize() 
    {
        this.camera.resize()
        this.renderer.resize()
    }

    update()
    {
        this.camera.update()
        this.world.update()
        this.renderer.update()
    }

    destroy()
    {
        this.size.off('resize')
        this.time.off('tick')

        // Destroy children
        this.scene.traverse((child) => {
            if (child instanceof THREE.Mesh)
            {
                child.geometry.dispose()

                for (const key in child.material)
                {
                    const value = child.material[key]

                    if (value && typeof value.dispose === 'function')
                    {
                        value.dispose()    
                    }
                }
            }
        })

        this.camera.controls.dispose()
        this.renderer.instance.dispose()
        if (this.debug.active)
        {
            this.debug.ui.destroy()
        }
    }
}