import * as THREE from "three"
import Experience from "./experience"

export default class Renderer 
{
    constructor()
    {
        this.experience = new Experience()
        this.canvas = this.experience.canvas
        this.size = this.experience.size
        this.scene = this.experience.scene
        this.camera = this.experience.camera

        this.setInstance()
    }

    setInstance()
    {
        this.instance = new THREE.WebGLRenderer({
            canvas: this.canvas,
            antialias: true
        })
        this.instance.physicallyCorrectLights = true
        this.instance.outputEncoding = THREE.sRGBEncoding
        this.instance.toneMapping = THREE.CineonToneMapping
        this.instance.toneMappingExposure = 1.75
        this.instance.shadowMap.enabled = true
        this.instance.shadowMap.type = THREE.PCFSoftShadowMap
        this.instance.setSize(this.size.width, this.size.height)
        this.instance.setPixelRatio(Math.min(this.size.pixelRatio, 2))
    }

    resize()
    {
        this.instance.setSize(this.size.width, this.size.height)
        this.instance.setPixelRatio(Math.min(this.size.pixelRatio, 2))
    }

    update()
    {
        this.instance.render(this.scene, this.camera.instance)
    }
}