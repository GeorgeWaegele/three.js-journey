import EventEmitter from "./EventEmitter"

export default class Time extends EventEmitter
{
    constructor()
    {
        super()

        // Setup
        this.start = Date.now()
        this.current = this.start
        this.elapsed = 0
        this.dt = 16

        window.requestAnimationFrame(() => {
            this.update()
        })
    }

    update()
    {
        const currentTime = Date.now()
        this.dt = currentTime - this.current
        this.current = currentTime
        this.elapsed = this.current - this.start

        this.trigger('update')

        window.requestAnimationFrame(() => {
            this.update()
        })
    }
}