varying float vRandom;
varying vec2 vUv;
varying float vElevation;

uniform sampler2D uTexture;
uniform vec3 uColor;

void main() 
{
    gl_FragColor = vec4(0.5, vRandom, 1.0, 1.0);

    // Texture
    vec4 textureColor = texture2D(uTexture, vUv);
    textureColor.rgb *= vElevation * 2.0 + 0.5;
    gl_FragColor = textureColor;
}